## Links
* https://www.liquidlight.co.uk/blog/article/building-a-vue-v2-js-app-using-vue-router/
* https://github.com/onort/vue-users/tree/master/src
* https://github.com/nuxt/nuxt.js

## Working State management
* https://github.com/NateTheGreatt/n8bit.tech

## PSD Mockups:
* http://www.instantshift.com/2015/04/15/free-apple-devices-mockup-psds/
* http://mooxidesign.com/best-devices-mockup-psd-templates-from-2016/
* http://www.cssauthor.com/outline-mockups/
* http://www.dreamstale.com/free-apple-devices-outline-vector-mockups/
* http://www.pixeden.com/psd-web-elements/vector-apple-outline-device-psd


<!-- What I'd recommend is to put all the projects into an object instead of an array, where property is a unique project ID. For example, in projects.data.js:

//...
projects: {
  testimonials: {
            name: 'Simple Fading Testimonials',
            // ...
  },
// ...

And in the template you can iterate the object like this:

<li v-for="(project, id) in projects.projects">
  <router-link :to="{ name: 'details', params: { projectId: id }}">{{ project.name }}</router-link>
</li>

This way, you will get URL like /project/testimonials/details and you can use $route.params.projectId as a key to fetch data from the projects object. -->
