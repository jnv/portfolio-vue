import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';

/* views */
import Home from '../components/home/Home';
import Projects from '../components/projects/Projects';
import About from '../components/about/About';
import Details from '../components/details/Details';


import CONSTANTS from '../utils/constants';
import store from '../store';

Vue.use(Router);
Vue.use(Meta, {
    keyName: 'metaInfo', // the component option name that vue-meta looks for meta info on.
    attribute: 'data-vue-meta', // the attribute name vue-meta adds to the tags it observes
    ssrAttribute: 'data-vue-meta-server-rendered', // the attribute name that lets vue-meta know that meta info has already been server-rendered
    tagIDKeyName: 'vmid' // the property name that vue-meta uses to determine whether to overwrite or append a tag
});


const router = new Router({
    mode: 'history',
    root: '/',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
        },
        {
            path: '/projects',
            name: 'Projects',
            component: Projects,
        },
        {
            path: '/project/:projectId/details',
            name: 'details',
            component: Details
        },
        {
            path: '/about',
            name: 'About',
            component: About,
        }
    ],
});

router.beforeEach((to, _, next) => {
    store.commit(CONSTANTS.MUTATIONS.ROUTE_CHANGE, to);
    next();
});

export default router;
