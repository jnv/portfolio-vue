export default {
    name: [
        'SK'
    ],
    summary: [
        'Test text here.'
    ],
    links: [
        {
            key: 'mail',
            text: 'Mail',
            icon: 'fi-mail',
            url: ''
        },
        {
            key: 'li',
            text: 'LinkedIn',
            icon: 'fi-social-linkedin',
            url: ''
        },
        {
            key: 'fb',
            text: 'Facebook',
            icon: 'fi-social-facebook',
            url: ''
        },
        {
            key: 'gh',
            text: 'GitHub',
            icon: 'fi-social-github',
            url: ''
        },
        {
            key: 'so',
            text: 'Stack Overflow',
            icon: 'fi-social-stack-overflow',
            url: ''
        }
    ]
};
