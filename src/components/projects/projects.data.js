export default {
    summary: [
        'This is the project summary text example'
    ],
    projects: {
        testimonials1: {
          name: 'Project 1',
          type: 'Wordpress Plugin',
          coding: ['PHP', 'jQuery'],
          design: true,
          development: true,
          url: 'https://en-gb.wordpress.org/plugins/simple-fading-testimonials-widget/',
          overview: '',
          description: 'Wordpress plugin that enables revolving and fading testimonials to be added to any Wordpress site. The plugin build utilises custom jQuery and PHP functionality. '
        },
        testimonials2: {
          name: 'Project 2',
          type: 'Wordpress Plugin',
          coding: ['PHP', 'jQuery'],
          design: true,
          development: true,
          url: 'https://en-gb.wordpress.org/plugins/simple-fading-testimonials-widget/',
          overview: '',
          description: 'Wordpress plugin that enables revolving and fading testimonials to be added to any Wordpress site. The plugin build utilises custom jQuery and PHP functionality. '
        }
    }
};
